# GIGA_PAINT
***
## NASZ_PROJEKT
Nasz projekt zdecydowanie będzie najlepszym z tworów w naszej klasie. Zdecydowanie przy tworzeniu jego uderzymy w kwintesencję problemu używając najbardziej optymalnych rozwiązań problemów aby dostarczyć nieskazitelny rezultat. Pozwoli nam to na zakrycie luki w rynku, która wytworzyła się w ostatnim czasie. Dzięki takiemu podejściu oraz wyśmienitej ambicji nas - twórców będziemy w stanie bezproblemowo tego dokonać i nie tylko stworzyć wyżej wymieniony projekt ale przede wszystkim wiele się z tego nauczyć. Właśnie wiedzą jest najwyższa wartością w życiu ludzkim, dlatego to jest to dla nas najcenniejsze w naszym przedsięwzięciu. 

## NASZA_MISJA
GigaPaint - w przeciwieństwie do tego, co sugerowałaby nazwa - nie będzie wcale giga. Wręcz przeciwnie. Nasze dzieło ma za zadanie odwołać się do dawnej prostoty programów graficznych, gdyż jak powszechnie wiadomo - "kiedyś to jednak było a dziś już nie". Nie można także zapomnieć o najważniejszej idei przyświecającej naszemu tworowi. Chcemy aby nasz program był możliwie lekki oraz miał tylko niezbędne użytkownikowi funkcje. W ten sposób chcemy rozwiązać problem ciągle narastającego ocieplenia klimatu. Coraz częściej na rynku pojawiają się ciężkie i rozbudowane programy, które mogłyby zdecydowanie być mniejsze. Na przykład MS Paint - czy ktokolwiek z nas na co dzień używa wszystkich funkcji tego oprogramowania? Założę się, że nie. Jestem pewien, że większość z nas nigdy nie stworzyła w nim kolorowego pioruna - więc po co tam ta funkcja? Zabiera ona jedynie cenną przestrzeń dyskową, w pamięci RAM oraz na ekranie. To takie podejście napędza dzisiejsze konsumpcjonistyczne podejście ludzi. Gdyby te funkcje nie istniały, to ludzie mogliby kupować mniejsze dyski, ekrany i pamięci RAM, dzięki czemu zatrzymalibyśmy globalne ocieplenie. Tu nawet nie chodzi o sam program, ale o nauczenie ludzi życia bez natłoku niepotrzebnych bodźców, takich jak wyżej wymieniony piorun czy narzędzie imitujące kredkę olejną. Ten projekt jest dla nas nie tylko zaliczeniem przedmiotu, ale większą misją duchową mającą na celu dołożenie cegiełki do ratowania planety. Nawet jeżeli zaoszczędzimy dzięki temu tylko kilka watów energii, to będzie to dla nas wielki sukces. 

## NASZA_DROGA
Aby osiągnąć ten cel musimy korzystać tylko z profesjonalnych narzędzi. Są to m.in. język C++ oraz kompilator Microsoft Visual C++, który jest liderem na rynku IT. Do tego będziemy korzystać także z Visual Studio 2019, które jest nie tylko edytorem kolorującym składnię, ale także wysoce rozbudowanym, nowoczesnym środowiskiem programistycznym.

---

![NASZE_MOTTO](https://th.bing.com/th/id/R.933a29114557ff6989a13c271057e481?rik=DGriJ91t0wt5wQ&riu=http%3a%2f%2fwww.wytrwali.pl%2fwp-content%2fuploads%2f2015%2f01%2fFontane_T_cytat.png&ehk=QeZ1ej8HXV4QOJCylB3H0fl%2fJ9VSk%2f37bfd%2ffZBL79I%3d&risl=&pid=ImgRaw&r=0)

---

>Ten projekt może zrewolucjonizować świat. - Adam Lisoń

>To się nie może nie udać - Jan Czyżewski

>Zbudujemy nowe trendy - Piotr Mądrzak
